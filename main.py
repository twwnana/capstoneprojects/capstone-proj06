import requests
import smtplib
import os
import paramiko
import schedule
import time

def sendNotification(msg):
  with smtplib.SMTP('smtp-mail.outlook.com', 587) as smtp:
    smtp.starttls()
    smtp.ehlo()
    smtp.login(os.environ.get("EMAIL_ADDRESS"), os.environ.get("EMAIL_PASSWORD"))
    
    smtp.sendmail(
      os.environ.get("EMAIL_ADDRESS"),
      "sheenismhael.lim@gmail.com",
      msg
    )
    smtp.close()
    
def restartAppContainer(ec2_endpoint, private_key):
  ssh_client = paramiko.SSHClient()
  ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

  ssh_client.connect(hostname=ec2_endpoint, port=22, username="ubuntu", key_filename=private_key)
  stdin, stdout, stderr = ssh_client.exec_command('sudo docker restart $(sudo docker container ls --all --quiet --filter "name=java-gradle-app")')
  
  ssh_client.close()

endpoint = input("Please enter the http endpoint of the running App (http://IP_or_DNS:8080): ")
ec2_endpoint = input("Please enter the EC2 Instance IP address running the App: ")
private_key = input("Enter path for the private key (will be used to SSH to the EC2 Endpoint): ")

def check_endpoint_status():  
  try:
    global endpoint
    response = requests.get(endpoint)

    if response.status_code == 200:
      print("Application is accessible at port 8080 is up.")
    else:
      print("Application is Down.")
      msg = f"Subject: SITE DOWN\nPlease check ASAP.\n\nApplication Returned {response.status_code}. Attempting to restart container."
      sendNotification(msg)
      restartAppContainer()
      
  except:
    msg = f"Subject: CRASHED - SITE DOWN\n\n"+"Looks like Server is not returning anything. Attempting to restart container."
    print(msg)
    print(f"EC2: {ec2_endpoint}, Key: {private_key}")
    sendNotification(msg)
    restartAppContainer(ec2_endpoint, private_key)

    print("Container has been restarted. Cooling off for 120 minutes.")
    time.sleep(120) # Wait for 2 minutes after the container has been restarted

schedule.every(5).seconds.do(check_endpoint_status)

while True:
  schedule.run_pending()