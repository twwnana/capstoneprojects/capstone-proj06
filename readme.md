# Introduction

This project is a sample implementation of all the knowledge learned in the DevOps Bootcamp from Techworld with Nana for Module 14.

The objectives here are similar to what is being asked on the Capstore Project 6.

<img width="650" alt="petclinic-screenshot" src="https://lh6.googleusercontent.com/7YR1kZOYKRuD6a3KV2uivjUMPnxtkAg2aznOOo6Damb-Q3mSpcb2o39zZV_uhcZvW69fy4gQewHJViW1J250MnI3DWitSlE73MG7EfZ2Ul1PMXr3jhrozehizVHxeDgeig=w501">

NOTE: For simplicity, the script is designed to monitor that was deployed in the capstone-projec05. Once the app is running on the EC2 instances on Capstone Project 05, we can use this python script for montioring.

The project is very similar to the contents below, the main differences are:
1. In addition to the HTTP endpoint to monitor, it also asks for the EC2 IP where the app is running.
2. It will ask you for the full path of the private key to connect to the EC2 instance.

Bootcamp Chapter 14 Git Repo: git@gitlab.com:twwnana/python/chapter14-lecture.git

# Sample Output
```
Please enter the http endpoint of the running App (http://IP_or_DNS:8080): http://999.999.999.999:8080
Please enter the EC2 Instance IP address running the App: 999.999.999.999
Enter path for the private key (will be used to SSH to the EC2 Endpoint): /home/sheenlim08/TWwN/Learning/TWwN/devops-bootcamp/capstone-proj/capstone-proj06/capstone-proj05-ansible.pem
Subject: CRASHED - SITE DOWN

Looks like Server is not returning anything. Attempting to restart container.
EC2: 18.141.192.101, Key: /home/sheenlim08/TWwN/Learning/TWwN/devops-bootcamp/capstone-proj/capstone-proj06/capstone-proj05-ansible.pem
Container has been restarted. Cooling off for 120 minutes.
Application is accessible at port 8080 is up.
Application is accessible at port 8080 is up.
Application is accessible at port 8080 is up.
```